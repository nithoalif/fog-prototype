module gitlab.com/nithoalif/fog-prototype

go 1.15

require (
	github.com/brianvoe/gofakeit/v6 v6.0.2
	github.com/google/uuid v1.2.0
	github.com/pebbe/zmq4 v1.2.2
	github.com/pkg/errors v0.9.1 // indirect
	github.com/subosito/gotenv v1.2.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
	golang.org/x/tools v0.0.0-20201105001634-bc3cf281b174 // indirect
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
)
