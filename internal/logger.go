package internal

import "go.uber.org/zap"

// Logger is the global logger used throughout the process
var Logger *zap.Logger

func init() {
	// initialize logger
	Logger, _ = zap.NewDevelopment()
	zap.ReplaceGlobals(Logger)
	defer Logger.Sync()
}
