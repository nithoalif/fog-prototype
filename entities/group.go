package entities

import (
	"time"

	"github.com/google/uuid"
)

type Group struct {
	ID             uuid.UUID   `json:"group_id"`
	CoachID        uuid.UUID   `json:"coach_id"`
	ParticipantIDs []uuid.UUID `json:"participants"`
	Timestamp      time.Time   `json:"timestamp"`
}
