package entities

// Alert can notify the participant
// if their heart rate is too high (also available for the coach)
// and if their pace is higher or lower than the caoch's pace or equal (-> goal)
type Alert struct {
	Type    AlertType `json:"type"`
	Message string    `json:"message"`
}

// AlertType defines the type of alert based on a certain heart rate threshold
type AlertType int

const (
	// HeartRateThreshold is the number of heart beats per minute for a person of 40 y.o. exercising in the middle of the anaerobic zone (Hardcore training)
	// We used the Fox and Haskell formula to find the constant (144 -> threshold to Anaerobic Zone, 162 -> threshold to VO2Max zone)
	// The diagram can be seen on the 'Heart rate' wikipedia page
	HeartRateThreshold int = 144 + ((162 - 144) / 2)
	// HeartRateHigh means the heart rate is higher than threshold
	HeartRateHigh AlertType = iota
	// PaceFast means the running pace is faster that the coach's
	PaceFast
	// PaceEqual means the running pace is equal that the coach's
	PaceEqual
	// PaceSlow means the running pace is slower that the coach's
	PaceSlow
)

// String of Alert returns a string describing the alert
func (al Alert) String() string {
	str := ""
	switch al.Type {
	case HeartRateHigh:
		str += "HeartRateHigh"
	case PaceFast:
		str += "PaceFast"
	case PaceEqual:
		str += "PaceEqual"
	case PaceSlow:
		str += "PaceSlow"
	}
	str += ": " + al.Message
	return str
}
