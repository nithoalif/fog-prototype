package entities

import (
	"math"
	"math/rand"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

func init() {
	faker := gofakeit.NewCrypto()
	gofakeit.SetGlobalFaker(faker)
}

// Measurement holds sensors data from running bracelet
type Measurement struct {
	Latitude  float64   `json:"latitude"`
	Longitude float64   `json:"longitude"`
	HeartRate int       `json:"heart_rate"`
	Timestamp time.Time `json:"timestamp"`
}

func NewMeasurement(last Measurement) Measurement {
	// simulate sensors measurement

	// randomize between 0 - 111m from the last measurement
	// https://www.usna.edu/Users/oceano/pguth/md_help/html/approx_equivalents.htm
	var dlat, dlong float64
	dlat = gofakeit.Float64Range(0, 0.01)
	dlong = gofakeit.Float64Range(0, 0.01)

	// if last measurement is empty, generate random coordinate in Berlin instead
	if last.Latitude == 0 && last.Longitude == 0 {
		dlat, _ = gofakeit.LatitudeInRange(52.33, 52.67)
		dlong, _ = gofakeit.LongitudeInRange(13.09, 13.75)
	}

	// heart rate sensor (simulation of heart rate between 75 to 200 beats/min)
	// https://en.wikipedia.org/wiki/Heart_rate#/media/File:Exercise_zones_Fox_and_Haskell.svg
	hr := rand.Intn((200-75)+1) + 75

	// encapsulate measurement data
	me := Measurement{
		Latitude:  last.Latitude + dlat,
		Longitude: last.Longitude + dlong,
		HeartRate: hr,
		Timestamp: time.Now(),
	}
	return me
}

// CalculatePace computes pace from 2 measurement (in minutes/km)
// Formula is taken from https://stackoverflow.com/a/21411693
func CalculatePace(m1, m2 Measurement) float64 {
	// Calculate distance
	lat1rad := m1.Latitude * math.Pi / 100
	lat2rad := m2.Latitude * math.Pi / 100
	lon1rad := m1.Longitude * math.Pi / 100
	lon2rad := m2.Longitude * math.Pi / 100

	R := 6371000.0 // earth radius in meters

	dLat := lat2rad - lat1rad
	dLon := lon2rad - lon1rad

	a := math.Sin(dLat/2)*math.Sin(dLat/2) +
		math.Cos(lat1rad)*math.Cos(lat2rad)*
			math.Sin(dLon/2)*math.Sin(dLon/2)

	distance := 2 * R * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	// Calculate elapsed time
	dTime := m2.Timestamp.Sub(m1.Timestamp)

	// Calculate pace
	return dTime.Minutes() / (distance / 1000)
}

// CalculateHeartRateDelta computes the difference between the heart rate passed asa parameter and the healthy heart rate constant
// Healthy heart rate => heart beats per minute for a person of 40 y.o. exercising in the middle of the anaerobic zone
func CalculateHeartRateDelta(heartRate int) int {
	return heartRate - HeartRateThreshold
}
