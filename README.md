# Fog Computing Prototype

# Documentation
- Document: http://bit.ly/fog-prototype2021-doc
- Video: http://bit.ly/fog-prototype2021-vid

# Prerequisite
- Go version >=1.14
- ZeroMQ >= 4.2

# Getting Started
- Clone project
    ```sh
    $ git clone git@gitlab.com:nithoalif/fog-prototype.git
    ```
- Go to the project directory
    ```sh
    $ cd fog-prototype
    ```
- Download the package dependencies
    ```sh
    $ GO111MODULE=on go mod vendor
    ```

# Usage
- Running the server
    ```sh
    $ go run ./main.go server
    2021-01-31T10:05:27.788+0100    INFO    fog-prototype/main.go:49        starting request-reply router at tcp://*:8001
    2021-01-31T10:05:27.788+0100    INFO    fog-prototype/main.go:61        starting alert publisher at tcp://*:8002
    ```
- Creating a group run
    ```sh
    $ go run ./main.go client create
    2021-01-31T10:05:31.523+0100    INFO    fog-prototype/main.go:138       client 042be545-f951-4b4e-b983-bf075d9f6e7f is connected to router at tcp://localhost:8001 and publisher at tcp://localhost:8002
    2021-01-31T10:05:31.534+0100    INFO    client/client.go:179    bracelet 042be545-f951-4b4e-b983-bf075d9f6e7f has joined group 2fd05e25-1760-4859-a26f-77df0a275018

    $ # the coach with bracelet id 042be545-f951-4b4e-b983-bf075d9f6e7f automatically join the group he/she just created
    $ # note the group_id = 2fd05e25-1760-4859-a26f-77df0a275018
    ```
- Joining a group run
    ```sh
    $ # use an existing group_id, eg: fce1bff7-bb66-4234-83f2-c47435adfbce

    $ go run ./main.go client join fce1bff7-bb66-4234-83f2-c47435adfbce
    2021-01-31T10:08:31.072+0100    INFO    fog-prototype/main.go:138       client bdb225c4-0bdd-474c-bce8-71b54e3dca8b is connected to router at tcp://localhost:8001 and publisher at tcp://localhost:8002
    2021-01-31T10:08:31.083+0100    INFO    client/client.go:179    bracelet bdb225c4-0bdd-474c-bce8-71b54e3dca8b has joined group fce1bff7-bb66-4234-83f2-c47435adfbce
    ```

# Author
- Nitho Alif Ibadurrahman
- Simine Kashi
