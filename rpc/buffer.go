package rpc

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sync"

	"github.com/google/uuid"
)

type Buffer interface {
	Add(Message) error
	Delete(uuid.UUID) error
	Get(uuid.UUID) (Message, error)
	GetN(int) []Message
	Flush() error
}

type buffer struct {
	fileName string
	buf      []Message
	idx      map[uuid.UUID]int
	mu       *sync.Mutex
}

// NewBuffer create a new buffer based on the specified file
func NewBuffer(fileName string) (Buffer, error) {
	buf := &buffer{
		fileName: fileName,
		buf:      []Message{},
		idx:      make(map[uuid.UUID]int),
		mu:       &sync.Mutex{},
	}

	// check previously created file
	_, err := os.Stat(fileName)
	if err == nil {
		// load file into memory
		file, err := os.OpenFile(fileName, os.O_RDONLY, 0600)
		if err != nil {
			return buf, err
		}
		defer file.Close()

		// parse json-formatted, new line-terminated text
		scanner := bufio.NewScanner(file)
		var msg Message
		msgs := []Message{}
		for scanner.Scan() {
			jsonText := scanner.Text()
			err := json.Unmarshal([]byte(jsonText), &msg)
			if err != nil {
				log.Error(err.Error())
			}
			msgs = append(msgs, msg)
		}

		// adjust index
		buf.buf = msgs
		for i, m := range msgs {
			buf.idx[m.ID] = i
		}
	}

	return buf, nil
}

func (b *buffer) Add(msg Message) error {
	b.mu.Lock()
	b.buf = append(b.buf, msg)
	b.idx[msg.ID] = len(b.buf) - 1
	b.mu.Unlock()
	return nil
}

func (b *buffer) Delete(id uuid.UUID) error {
	// check presence
	if !b.present(id) {
		return errors.New("invalid message")
	}

	// get index
	b.mu.Lock()
	i := b.idx[id]
	last := b.buf[len(b.buf)-1]

	// delete from buffer
	b.buf[i] = last              // copy last element to index i
	b.buf = b.buf[:len(b.buf)-1] // truncate last element
	b.idx[last.ID] = i           // assign new index to the previously last element

	delete(b.idx, id)
	b.mu.Unlock()

	return nil
}

func (b *buffer) Get(id uuid.UUID) (Message, error) {
	// check presence
	if !b.present(id) {
		return Message{}, errors.New("invalid message")
	}

	i := b.idx[id]
	return b.buf[i], nil
}

func (b *buffer) GetN(n int) []Message {
	if n > len(b.buf) {
		n = len(b.buf)
	}

	msgs := []Message{}
	for i := 0; i < n; i++ {
		msgs = append(msgs, b.buf[i])
	}
	return msgs
}

func (b *buffer) Flush() error {
	file, err := os.Create(b.fileName)
	if err != nil {
		return err
	}
	defer file.Close()

	// skip disk access if buffer is empty
	if len(b.buf) == 0 {
		return nil
	}

	log.Info("data has been flushed to disk")
	w := bufio.NewWriter(file)
	for _, m := range b.buf {
		jsonBytes, err := json.Marshal(m)
		if err != nil {
			log.Error(err.Error())
		}
		fmt.Fprintln(w, string(jsonBytes))
	}

	return w.Flush()
}

func (b *buffer) present(id uuid.UUID) bool {
	if _, ok := b.idx[id]; ok {
		return true
	}
	return false
}
