package rpc

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"github.com/google/uuid"
	zmq "github.com/pebbe/zmq4"
	"gitlab.com/nithoalif/fog-prototype/internal"
)

var log = internal.Logger

// Message holds internal message representation that can be sent into the wire
type Message struct {
	ID            uuid.UUID              `json:"id"`
	Source        uuid.UUID              `json:"source"`
	MessageType   MessageType            `json:"type"`
	OperationType OperationType          `json:"operation"`
	Payload       map[string]interface{} `json:"payload"`
	Timestamp     time.Time              `json:"timestamp"`
}

// MessageType represents the type of message that can be sent into the wire
type MessageType int

const (
	// MessageSyn is tipically for request
	MessageSyn MessageType = iota
	// MessageAck is acknowledgement
	MessageAck
)

// OperationType represents the type of operation that can be sent into the wire
type OperationType int

const (
	// OperationCreateGroup is used to call the corresponding create group procedure
	OperationCreateGroup OperationType = iota
	// OperationJoinGroup is used to call the corresponding join group procedure
	OperationJoinGroup
	// OperationSendMeasurement is used to call the corresponding send/receive measurement procedure
	OperationSendMeasurement
	// OperationSendHRAlert is used to call the corresponding send/receive heart rate alert procedure
	OperationSendAlert
)

// SendMessage serializes message into a JSON representation and sends it the ZeroMQ socket
func SendMessage(socket *zmq.Socket, msg Message) error {
	id, err := socket.GetIdentity()
	if err != nil {
		return err
	}
	src, err := uuid.Parse(id)
	if err != nil {
		return err
	}
	msg.Source = src

	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	// simulate message loss
	switch r := rand.Intn(4); r {
	case 0:
		log.Warn(fmt.Sprintf("Message %s is lost", msg.ID))
	default:
		log.Debug(fmt.Sprintf("SEND %s", string(jsonMsg)))
		_, err = socket.SendMessage(jsonMsg)
		if err != nil {
			return err
		}
	}

	return nil
}

// SendMessageTo serializes message into a JSON representation and sends it to either to a specific clientID or topic subscription in the ZeroMQ socket
func SendMessageTo(socket *zmq.Socket, msg Message, dest uuid.UUID) error {
	jsonMsg, err := json.Marshal(msg)
	if err != nil {
		return nil
	}

	log.Debug(fmt.Sprintf("SEND %s", string(jsonMsg)))

	// simulate message loss
	switch r := rand.Intn(3); r {
	case 0:
		log.Warn(fmt.Sprintf("Message %s is lost", msg.ID))
	default:
		_, err = socket.SendMessage(dest, jsonMsg)
		if err != nil {
			return err
		}
	}

	return nil
}

// RecvMessage receives and deserializes JSON representation into a message from ZeroMQ socket
func RecvMessage(socket *zmq.Socket) (Message, error) {
	jsonMsg, err := socket.RecvMessage(zmq.DONTWAIT)
	if err != nil {
		return Message{}, nil
	}

	log.Debug(fmt.Sprintf("RECV %s", jsonMsg))

	payload := jsonMsg[0]
	if len(jsonMsg) > 1 {
		payload = jsonMsg[1]
	}

	msg := Message{}
	err = json.Unmarshal([]byte(payload), &msg)
	if err != nil {
		return Message{}, nil
	}

	return msg, nil
}
