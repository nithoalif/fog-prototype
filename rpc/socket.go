package rpc

import (
	"github.com/google/uuid"
	zmq "github.com/pebbe/zmq4"
)

type SockOpts struct {
	ZmqType      zmq.Type
	SockAddress  string
	ClientID     uuid.UUID
	AuthDomain   string
	AuthUsername string
	AuthPassword string
}

func NewServerSocket(opts SockOpts) (*zmq.Socket, error) {
	// setup plain-text auth
	isSecured := opts.AuthUsername != "" && opts.AuthPassword != ""
	if isSecured {
		zmq.AuthSetVerbose(true)
		zmq.AuthAllow(opts.AuthDomain)
		zmq.AuthPlainAdd(opts.AuthDomain, opts.AuthUsername, opts.AuthPassword)
	}

	// setup socket
	sock, err := zmq.NewSocket(opts.ZmqType)
	if err != nil {
		return nil, nil
	}
	if isSecured {
		sock.ServerAuthPlain(opts.AuthDomain)
	}
	err = sock.Bind(opts.SockAddress)
	if err != nil {
		return nil, nil
	}

	return sock, nil
}

func NewClientSocket(opts SockOpts) (*zmq.Socket, error) {
	// setup socket
	sock, err := zmq.NewSocket(opts.ZmqType)
	if err != nil {
		return nil, nil
	}

	// setup plain-text auth
	isSecured := opts.AuthUsername != "" && opts.AuthPassword != ""
	if isSecured {
		sock.SetPlainUsername(opts.AuthUsername)
		sock.SetPlainPassword(opts.AuthUsername)
	}

	// assign client id
	err = sock.SetIdentity(opts.ClientID.String())

	err = sock.Connect(opts.SockAddress)
	if err != nil {
		return nil, nil
	}

	return sock, nil
}
