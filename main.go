package main

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/google/uuid"
	zmq "github.com/pebbe/zmq4"
	"github.com/subosito/gotenv"
	"gitlab.com/nithoalif/fog-prototype/client"
	"gitlab.com/nithoalif/fog-prototype/internal"
	"gitlab.com/nithoalif/fog-prototype/rpc"
	"gitlab.com/nithoalif/fog-prototype/server"
)

var (
	MeasurementInterval = 5  // in seconds
	RetryInterval       = 5  // in seconds
	AlertInterval       = 5  // in seconds
	FlushInterval       = 10 //in seconds
	ServerHost          = "localhost"
	ServerPort          = "8001"
	ClientDB            = "client.db"
	ServerDB            = "server.db"
)

var log = internal.Logger

func init() {
	gotenv.Load()
	if os.Getenv("CLIENT_DB") != "" {
		ClientDB = os.Getenv("CLIENT_DB")
	}
	if os.Getenv("SERVER_DB") != "" {
		ClientDB = os.Getenv("SERVER_DB")
	}
	if os.Getenv("SERVER_HOST") != "" {
		ClientDB = os.Getenv("SERVER_HOST")
	}
	if os.Getenv("SERVER_PORT") != "" {
		ClientDB = os.Getenv("SERVER_PORT")
	}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("incorrect run type")
	}
	if os.Args[1] == "server" {
		serverRun()
	} else if os.Args[1] == "client" {
		clientRun()
	}

}

func serverRun() {
	// setup client-facing request-response router
	opts := rpc.SockOpts{
		ZmqType:     zmq.ROUTER,
		SockAddress: "tcp://*:" + ServerPort,
	}
	router, err := rpc.NewServerSocket(opts)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer router.Close()
	log.Info(fmt.Sprintf("starting request-reply router at %s", opts.SockAddress))

	// prepare buffer
	buf, err := rpc.NewBuffer(ServerDB)
	if err != nil {
		log.Fatal(err.Error())
	}

	// create server
	s, err := server.NewServer(router, &buf)
	if err != nil {
		log.Fatal(err.Error())
	}

	// setup request processor
	var mu sync.Mutex
	go func() {
		for {
			time.Sleep(10 * time.Millisecond)
			mu.Lock()
			// receive client request
			msg, err := rpc.RecvMessage(router)
			if err != nil {
				log.Error(err.Error())
			}
			if msg.ID != uuid.Nil {
				s.ProcessMessage(msg)
			}
			mu.Unlock()
		}
	}()

	// setup alert pusher
	go func() {
		for {
			time.Sleep(time.Duration(AlertInterval) * time.Second)
			mu.Lock()
			err = s.SendAlert()
			if err != nil {
				log.Error(err.Error())
			}
			mu.Unlock()
		}
	}()

	// setup message flusher
	go setupMessageFlusher(&buf)

	// setup message retrier
	for {
		time.Sleep(time.Duration(RetryInterval) * time.Second)
		mu.Lock()
		s.ProcessUnackMessage()
		mu.Unlock()
	}

}

func clientRun() {

	clientID := uuid.New()

	// setup dealer socket
	dealerOpts := rpc.SockOpts{
		ZmqType:     zmq.DEALER,
		ClientID:    clientID,
		SockAddress: fmt.Sprintf("tcp://%s:%s", ServerHost, ServerPort),
	}
	dealer, err := rpc.NewClientSocket(dealerOpts)
	if err != nil {
		log.Fatal(err.Error())
	}
	defer dealer.Close()

	// check bracelet id
	braceletID, err := dealer.GetIdentity()
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Info(fmt.Sprintf("client %s is connected to router at %s", braceletID, dealerOpts.SockAddress))

	// prepare buffer
	buf, err := rpc.NewBuffer(ClientDB)
	if err != nil {
		log.Fatal(err.Error())
	}

	// create client
	cl, err := client.NewClient(dealer, &buf)
	if err != nil {
		log.Fatal(err.Error())
	}

	// parse arguments
	if len(os.Args) < 3 {
		log.Fatal("insufficient argument")
	}
	if os.Args[2] == "create" {
		// create group run
		err := cl.SendCreateGroup()
		if err != nil {
			log.Error(err.Error())
		}
		log.Info("create group request has been sent")
	} else if os.Args[2] == "join" && len(os.Args) > 3 {
		// join group run
		groupID, err := uuid.Parse(os.Args[3])
		if err != nil {
			log.Fatal(err.Error())
		}
		err = cl.SendJoinGroup(groupID)
		if err != nil {
			log.Error(err.Error())
		}
	}

	// setup request processor
	var mu sync.Mutex
	go func() {
		for {
			time.Sleep(10 * time.Millisecond)
			mu.Lock()
			// receive client request
			msg, err := rpc.RecvMessage(dealer)
			if err != nil {
				log.Error(err.Error())
			}
			if msg.ID != uuid.Nil {
				cl.ProcessMessage(msg)
			}
			mu.Unlock()
		}
	}()

	// setup measurement pusher
	go func() {
		var mu sync.Mutex
		for {
			time.Sleep(time.Duration(MeasurementInterval) * time.Second)
			mu.Lock()
			err = cl.SendMeasurement()
			if err != nil {
				log.Error(err.Error())
			}
			mu.Unlock()
		}
	}()

	// setup message flusher
	go setupMessageFlusher(&buf)

	// setup message retrier
	for {
		time.Sleep(time.Duration(RetryInterval) * time.Second)
		mu.Lock()
		cl.ProcessUnackMessage()
		mu.Unlock()
	}
}

func setupMessageFlusher(buf *rpc.Buffer) {
	for {
		time.Sleep(time.Duration(FlushInterval) * time.Second)
		err := (*buf).Flush()
		if err != nil {
			log.Error(err.Error())
		}
	}
}
