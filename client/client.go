package client

import (
	"encoding/json"
	"errors"
	"fmt"
	"sync"
	"time"

	"github.com/google/uuid"
	zmq "github.com/pebbe/zmq4"
	"gitlab.com/nithoalif/fog-prototype/entities"
	"gitlab.com/nithoalif/fog-prototype/internal"
	"gitlab.com/nithoalif/fog-prototype/rpc"
)

var log = internal.Logger

// Client holds client operation contracts
type Client interface {
	ProcessMessage(rpc.Message)
	ProcessUnackMessage()
	SendCreateGroup() error
	SendJoinGroup(uuid.UUID) error
	SendMeasurement() error
}

type client struct {
	braceletID      uuid.UUID
	groupID         uuid.UUID
	lastMeasurement entities.Measurement
	msgBuffer       *rpc.Buffer
	socket          *zmq.Socket
	mu              *sync.Mutex
}

// NewClient initialize a new client
func NewClient(dealerSocket *zmq.Socket, buf *rpc.Buffer) (Client, error) {
	// parse bracelet id
	socketID, err := dealerSocket.GetIdentity()
	if err != nil {
		return client{}, err
	}
	braceletID, err := uuid.Parse(socketID)
	if err != nil {
		return client{}, err
	}

	me := entities.Measurement{}

	// initialize message buffer
	// mBuf := internal.NewBuffer()

	return client{
		braceletID:      braceletID,
		msgBuffer:       buf,
		lastMeasurement: me,
		socket:          dealerSocket,
		mu:              &sync.Mutex{},
	}, nil
}

// ProcessMessage determines which function should be called
func (c client) ProcessMessage(msg rpc.Message) {
	if msg.MessageType == rpc.MessageAck {
		// check buffer
		m, err := (*c.msgBuffer).Get(msg.ID)
		if err != nil {
			// possibly has been acknowledged
			log.Warn(err.Error())
		}

		// assign group
		if m.OperationType == rpc.OperationCreateGroup ||
			m.OperationType == rpc.OperationJoinGroup {
			err := c.assignGroup(msg)
			if err != nil {
				log.Error(err.Error())
			}
		}

		// process ack
		c.receiveAck(msg)
	}

	if msg.OperationType == rpc.OperationSendAlert && msg.MessageType == rpc.MessageSyn {
		err := c.receiveAlert(msg)
		if err != nil {
			log.Error(err.Error())
		}
	}

}

// ProcessUnackMessage retries sending unacknowledged message(s)
func (c client) ProcessUnackMessage() {
	// get 5 message
	msgs := (*c.msgBuffer).GetN(5)
	minElapsed := 5 * time.Second

	for _, msg := range msgs {
		go func(m rpc.Message) {
			// skip if too soon
			if time.Now().Sub(m.Timestamp) < minElapsed {
				return
			}

			// retry send
			log.Info(fmt.Sprintf("message %s has not been acknowledged, retrying", m.ID))
			c.mu.Lock()
			err := rpc.SendMessage(c.socket, m)
			if err != nil {
				log.Error(err.Error())
			}
			c.mu.Unlock()

		}(msg)
	}
}

// SendCreateGroup send join group request
func (c client) SendCreateGroup() error {
	// send create group request
	payload := make(map[string]interface{})
	payload["coach_id"] = c.braceletID

	msg := rpc.Message{
		ID:            uuid.New(),
		MessageType:   rpc.MessageSyn,
		OperationType: rpc.OperationCreateGroup,
		Payload:       payload,
		Timestamp:     time.Now(),
	}

	c.mu.Lock()
	err := (*c.msgBuffer).Add(msg)
	if err != nil {
		return err
	}
	err = rpc.SendMessage(c.socket, msg)
	c.mu.Unlock()

	return err
}

// SendJoin send join group request
func (c client) SendJoinGroup(groupID uuid.UUID) error {
	payload := make(map[string]interface{})
	payload["group_id"] = groupID
	payload["participant_id"] = c.braceletID

	msg := rpc.Message{
		ID:            uuid.New(),
		MessageType:   rpc.MessageSyn,
		OperationType: rpc.OperationJoinGroup,
		Payload:       payload,
		Timestamp:     time.Now(),
	}

	c.mu.Lock()
	(*c.msgBuffer).Add(msg)
	err := rpc.SendMessage(c.socket, msg)
	c.mu.Unlock()

	return err
}

// SendMeasurement send simulated measurement data
func (c client) SendMeasurement() error {
	me := entities.NewMeasurement(c.lastMeasurement)
	meJSON, err := json.Marshal(me)
	if err != nil {
		return err
	}

	// construct message
	payload := make(map[string]interface{})
	payload["measurement"] = string(meJSON)
	msg := rpc.Message{
		ID:            uuid.New(),
		MessageType:   rpc.MessageSyn,
		OperationType: rpc.OperationSendMeasurement,
		Payload:       payload,
		Timestamp:     time.Now(),
	}

	// send measurement data
	c.mu.Lock()
	c.lastMeasurement = me
	(*c.msgBuffer).Add(msg)
	err = rpc.SendMessage(c.socket, msg)
	c.mu.Unlock()

	return err
}

func (c client) assignGroup(msg rpc.Message) error {
	// check for error
	if val, ok := msg.Payload["error"]; ok {
		return errors.New(val.(string))
	}

	// receive create group reply
	groupID, err := uuid.Parse(msg.Payload["group_id"].(string))
	if err != nil {
		return err
	}
	c.groupID = groupID
	log.Info(fmt.Sprintf("bracelet %s has joined group %s", c.braceletID, c.groupID))

	return nil
}

func (c client) receiveAck(msg rpc.Message) {
	err := (*c.msgBuffer).Delete(msg.ID)
	if err != nil {
		log.Warn(fmt.Sprintf("dropping message %s, it has been acknowledged before", msg.ID))
		// log.Error(err.Error())
	}
	log.Info(fmt.Sprintf("message %s has been acknowledged", msg.ID))
}

func (c client) receiveAlert(msg rpc.Message) error {
	// send ack reply
	reply := rpc.Message{
		ID:          msg.ID,
		MessageType: rpc.MessageAck,
	}
	err := rpc.SendMessage(c.socket, reply)
	if err != nil {
		return err
	}

	// log alert
	al := entities.Alert{}
	alertJSON := msg.Payload["alert"]
	json.Unmarshal([]byte(alertJSON.(string)), &al)
	log.Info(al.String())
	return nil
}
