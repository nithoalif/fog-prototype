package server

import (
	"encoding/json"
	"fmt"
	"sort"
	"sync"
	"time"

	"github.com/google/uuid"
	zmq "github.com/pebbe/zmq4"
	"gitlab.com/nithoalif/fog-prototype/entities"
	"gitlab.com/nithoalif/fog-prototype/internal"
	"gitlab.com/nithoalif/fog-prototype/rpc"
)

var log = internal.Logger

// Server holds server service contracts
type Server interface {
	ProcessMessage(rpc.Message)
	ProcessUnackMessage()
	SendAlert() error
}

type server struct {
	groupList       map[uuid.UUID]entities.Group
	measurementList map[uuid.UUID][]entities.Measurement
	msgBuffer       *rpc.Buffer
	socket          *zmq.Socket
	mu              *sync.Mutex
}

// NewServer initialize a new server
func NewServer(routerSocket *zmq.Socket, buf *rpc.Buffer) (Server, error) {
	groupList := make(map[uuid.UUID]entities.Group)
	measurementList := make(map[uuid.UUID][]entities.Measurement)

	return server{
		groupList:       groupList,
		measurementList: measurementList,
		msgBuffer:       buf,
		socket:          routerSocket,
		mu:              &sync.Mutex{},
	}, nil
}

// ProcessMessage determines which function should be called
func (s server) ProcessMessage(msg rpc.Message) {
	if msg.MessageType == rpc.MessageAck {
		// receive ack
		s.receiveAck(msg)
		log.Info(fmt.Sprintf("message %s has been acknowledged", msg.ID))
	}

	if msg.MessageType == rpc.MessageSyn {
		// receive request
		var err error
		switch msg.OperationType {
		case rpc.OperationCreateGroup:
			err = s.receiveCreateGroup(msg)
		case rpc.OperationJoinGroup:
			err = s.receiveJoinGroup(msg)
		case rpc.OperationSendMeasurement:
			err = s.receiveMeasurement(msg)
		}
		if err != nil {
			log.Error(err.Error())
		}
	}

}

// ProcessUnackMessage retries sending unacknowledged message(s)
func (s server) ProcessUnackMessage() {
	// get 5 message
	msgs := (*s.msgBuffer).GetN(5)
	minElapsed := 5 * time.Second

	for _, msg := range msgs {
		go func(m rpc.Message) {
			// skip if too soon
			if time.Now().Sub(m.Timestamp) < minElapsed {
				return
			}

			// retry send
			log.Info(fmt.Sprintf("message %s has not been acknowledged, retrying", m.ID))
			s.mu.Lock()
			err := rpc.SendMessageTo(s.socket, m, m.Source)
			if err != nil {
				log.Error(err.Error())
			}
			s.mu.Unlock()

		}(msg)
	}
}

// SendAlert sends heart rate & pace alert to each client (HR for everybody, pace only for particpant)
func (s server) SendAlert() error {
	var wg sync.WaitGroup
	wg.Add(len(s.groupList))

	for _, group := range s.groupList {
		go func(g entities.Group) {
			defer wg.Done()

			// check coach's pace
			coachList := s.measurementList[g.CoachID]
			if len(coachList) < 2 {
				return
			}
			// sort measurement by timestamp
			sort.Slice(coachList, func(i, j int) bool {
				return coachList[i].Timestamp.Before(coachList[j].Timestamp)
			})
			last := coachList[len(coachList)-1]
			last2 := coachList[len(coachList)-2]
			paceCoach := entities.CalculatePace(last2, last)

			// send heart rate alert to the coach
			err := s.sendAlertHR(last, g.CoachID)
			if err != nil {
				log.Error(err.Error())
			}

			// check participants' data
			for _, pid := range g.ParticipantIDs {
				pList := s.measurementList[pid]
				if len(pList) < 1 {
					return
				}

				// may not be last timestamp-wise, but it is ok since the measurement interval is relatively short
				pLast := pList[len(pList)-1]
				pLast2 := pList[len(pList)-2]

				// send heart rate alert to every participant
				err := s.sendAlertHR(last, g.CoachID)
				if err != nil {
					log.Error(err.Error())
				}

				pPace := entities.CalculatePace(pLast2, pLast)
				var alertPace entities.Alert
				if pPace > paceCoach {
					alertPace = entities.Alert{
						Type:    entities.PaceFast,
						Message: "Your pace is too fast, slow down!",
					}
				} else if pPace < paceCoach {
					alertPace = entities.Alert{
						Type:    entities.PaceSlow,
						Message: "Your pace is too slow, run faster!",
					}
				} else {
					alertPace = entities.Alert{
						Type:    entities.PaceEqual,
						Message: "Your pace is perfect, you are doing great!",
					}
				}

				// construct pace message for pace alert
				payload := make(map[string]interface{})
				payload["alert"] = alertPace
				msg := rpc.Message{
					ID:            uuid.New(),
					MessageType:   rpc.MessageSyn,
					OperationType: rpc.OperationSendAlert,
					Payload:       payload,
				}

				// send pace alert to every participant
				s.mu.Lock()
				err = rpc.SendMessageTo(s.socket, msg, pid)
				if err != nil {
					log.Error(err.Error())
				}
				s.mu.Unlock()

			}
		}(group)
	}
	return nil
}

func (s server) sendAlertHR(m entities.Measurement, dest uuid.UUID) error {
	deltaHR := entities.CalculateHeartRateDelta(m.HeartRate)
	if deltaHR > 0 {
		log.Debug("AAAA")
		// construct alert json
		alertHR := entities.Alert{
			Type:    entities.HeartRateHigh,
			Message: "Your heart rate is too high, take a rest!",
		}
		ahrJSON, err := json.Marshal(alertHR)
		if err != nil {
			return err
		}

		log.Debug("BBB")

		// construct alert message
		payload := make(map[string]interface{})
		payload["alert"] = string(ahrJSON)
		msg := rpc.Message{
			ID:            uuid.New(),
			MessageType:   rpc.MessageSyn,
			OperationType: rpc.OperationSendAlert,
			Payload:       payload,
		}
		log.Debug("CCC")

		// send to the participant
		s.mu.Lock()
		(*s.msgBuffer).Add(msg)
		err = rpc.SendMessageTo(s.socket, msg, dest)
		if err != nil {
			return err
		}
		log.Debug("DDD")

		s.mu.Unlock()
	}
	return nil
}

func (s server) receiveCreateGroup(msg rpc.Message) error {
	// construct ack reply
	reply := rpc.Message{
		ID:          msg.ID,
		MessageType: rpc.MessageAck,
		Payload:     make(map[string]interface{}),
	}

	// parse coachID
	coachID, err := uuid.Parse(msg.Payload["coach_id"].(string))
	if err != nil {
		return s.replyWithError(err, reply, msg.Source)
	}

	// create group and record it
	g := entities.Group{
		ID:             uuid.New(),
		Timestamp:      time.Now(),
		CoachID:        coachID,
		ParticipantIDs: []uuid.UUID{},
	}
	s.groupList[g.ID] = g
	log.Info(fmt.Sprintf("coach %s has created group %s", coachID.String(), g.ID.String()))

	// send reply
	// since reply == ack, no need to add it to buffer
	reply.Payload["group_id"] = g.ID
	s.mu.Lock()
	errs := rpc.SendMessageTo(s.socket, reply, msg.Source)
	s.mu.Unlock()

	return errs
}

func (s server) receiveJoinGroup(msg rpc.Message) error {
	// construct ack reply
	reply := rpc.Message{
		ID:          msg.ID,
		MessageType: rpc.MessageAck,
		Payload:     make(map[string]interface{}),
	}

	// check groupID
	groupID, err := uuid.Parse(msg.Payload["group_id"].(string))
	if err != nil {
		return s.replyWithError(err, reply, msg.Source)
	}
	if _, ok := s.groupList[groupID]; !ok {
		err = fmt.Errorf("group %s does not exist", groupID)
		return s.replyWithError(err, reply, msg.Source)
	}

	// check participantID existence in the group
	partipantID, err := uuid.Parse(msg.Payload["participant_id"].(string))
	if err != nil {
		return s.replyWithError(err, reply, msg.Source)
	}
	for _, v := range s.groupList[groupID].ParticipantIDs {
		if v == partipantID {
			err = fmt.Errorf("participant %s is already in the group %s", partipantID, groupID)
			return s.replyWithError(err, reply, msg.Source)
		}
	}

	// join participant to the group and record it
	g := s.groupList[groupID]
	g.ParticipantIDs = append(g.ParticipantIDs, partipantID)
	s.groupList[groupID] = g
	log.Info(fmt.Sprintf("participant %s has been added to the group %s", partipantID, groupID))

	// send reply
	// since reply == ack, no need to add it to buffer
	reply.Payload["group_id"] = groupID
	s.mu.Lock()
	errs := rpc.SendMessageTo(s.socket, reply, msg.Source)
	s.mu.Unlock()

	return errs
}

func (s server) receiveMeasurement(msg rpc.Message) error {
	// construct ack reply
	reply := rpc.Message{
		ID:          msg.ID,
		MessageType: rpc.MessageAck,
		Payload:     make(map[string]interface{}),
	}

	// record measurement
	data := entities.Measurement{}
	err := json.Unmarshal([]byte(msg.Payload["measurement"].(string)), &data)
	if err != nil {
		s.replyWithError(err, reply, msg.Source)
	}

	// send reply
	// since reply == ack, no need to add it to buffer
	s.mu.Lock()
	s.measurementList[msg.Source] = append(s.measurementList[msg.Source], data)
	errs := rpc.SendMessageTo(s.socket, reply, msg.Source)
	s.mu.Unlock()

	return errs
}

func (s server) receiveAck(msg rpc.Message) {
	err := (*s.msgBuffer).Delete(msg.ID)
	if err != nil {
		log.Error(err.Error())
	}
	log.Info(fmt.Sprintf("message %s has been acknowledged", msg.ID))
}

func (s server) replyWithError(err error, reply rpc.Message, dest uuid.UUID) error {
	log.Error(err.Error())
	reply.Payload["error"] = err.Error()

	s.mu.Lock()
	(*s.msgBuffer).Add(reply)
	errs := rpc.SendMessageTo(s.socket, reply, dest)
	s.mu.Unlock()

	return errs
}
